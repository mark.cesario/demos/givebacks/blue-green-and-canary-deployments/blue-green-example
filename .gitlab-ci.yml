image: "registry.gitlab.com/gitlab-org/cluster-integration/auto-deploy-image:v0.16.1"

variables:
  DOCKER_DRIVER: overlay2 # https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-the-overlayfs-driver-per-project
  ROLLOUT_RESOURCE_TYPE: deployment # https://docs.gitlab.com/ee/topics/autodevops/customize.html#build-and-deployment
  DOCKER_TLS_CERTDIR: "" # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501

stages:
  - build
  - deploy
  - switch

include:
  - template: Jobs/Build.gitlab-ci.yml

.deploy_template: &deploy_template
  stage: deploy
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: manual
      allow_failure: true
  script:
    # Force auto-deploy script to use a different release name, this can only
    # be done if you DISABLE `GitLab-managed cluster` in your Kubernetes
    # clusters provider details settings. In the future GitLab will nativly
    # support green/blue deployments and this will no longer be required.
    - export HELM_RELEASE_NAME="$BLUE_GREEN_DEPLOY"
    # Determine if this deployment is already currently active.
    - >
        export BLUE_GREEN_IS_ACTIVE="$(
          if VALUE=$(kubectl -n "$KUBE_NAMESPACE" \
                        get deploy "$HELM_RELEASE_NAME" \
                        -o=jsonpath='{.metadata.annotations.bluegreen/live}'); then
            echo "$VALUE"
          else
            echo "false"
          fi
        )"
    # If it is, then we must ensure the live production url is included.
    - >
        if [ "$BLUE_GREEN_IS_ACTIVE" = "true" ]; then
          export ADDITIONAL_HOSTS="$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN"
        fi
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy initialize_tiller
    - auto-deploy create_secret
    - auto-deploy deploy
    - auto-deploy persist_environment_url
  artifacts:
    paths: [environment_url.txt]
  dependencies:
    - build
  environment:
    name: "$BLUE_GREEN_DEPLOY"
    url: http://$BLUE_GREEN_DEPLOY-$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
    kubernetes:
      namespace: production

deploy to blue:
  <<: *deploy_template
  variables:
    BLUE_GREEN_DEPLOY: blue

deploy to green:
  <<: *deploy_template
  variables:
    BLUE_GREEN_DEPLOY: green

.switch_template: &switch_template
  stage: switch
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: manual
      allow_failure: true
  script:
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy initialize_tiller
    # Disable default provisioning of PostgreSQL
    - export POSTGRES_ENABLED="false"
    # At the time of this writing, the auto-deploy script does not support the
    # notion of green/blue deployments. It only has tracks stable and rollout.
    # In the future green and blue should be implemented as tracks so that you
    # can do incremental rollouts between them. Also note that with this setup,
    # each deployment will trigger the GitLab managed cert-manager to issue a
    # new certificate. This is not ideal as there should only be one certiface
    # issued for the "production" environment, which includes blue and green.
    # Blue and green should both be considered the "production" environment.
    - export HELM_HOST="localhost:44134"
    # Mark this deployment as active:
    - >
      helm upgrade --reuse-values \
        --wait \
        --set service.additionalHosts="{$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN}" \
        --namespace="$KUBE_NAMESPACE" \
        "$BLUE_GREEN_ACTIVATE" \
        chart/
    - >
        kubectl -n "$KUBE_NAMESPACE" \
          patch deploy "$BLUE_GREEN_ACTIVATE" \
          --type json \
          -p='[{"op": "replace", "path": "/metadata/annotations/bluegreen~1live", "value": "true"}]'
    # Then mark the other deployment as NOT active (if it is actually deployed):
    - >
        export DEPLOYED="$(
          if kubectl -n "$KUBE_NAMESPACE" get deploy "$BLUE_GREEN_DEACTIVATE" >/dev/null 2>&1; then
            echo "true"
          else
            echo "false"
          fi
        )"
    - kubectl -n "$KUBE_NAMESPACE" get deploy "$BLUE_GREEN_DEACTIVATE" || true
    - >
        if [ "$DEPLOYED" = "true" ]; then
          helm upgrade --reuse-values \
            --wait \
            --set service.additionalHosts="" \
            --namespace="$KUBE_NAMESPACE" \
            "$BLUE_GREEN_DEACTIVATE" \
            chart/
        fi
    - >
        if [ "$DEPLOYED" = "true" ]; then
          kubectl -n "$KUBE_NAMESPACE" \
            patch deploy "$BLUE_GREEN_DEACTIVATE" \
            --type json \
            -p='[{"op": "replace", "path": "/metadata/annotations/bluegreen~1live", "value": "false"}]'
        fi
  environment:
    name: production
    kubernetes:
      namespace: production

switch to blue:
  <<: *switch_template
  variables:
    BLUE_GREEN_ACTIVATE: blue
    BLUE_GREEN_DEACTIVATE: green

switch to green:
  <<: *switch_template
  variables:
    BLUE_GREEN_ACTIVATE: green
    BLUE_GREEN_DEACTIVATE: blue
